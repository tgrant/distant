﻿namespace TGrant.Distant.Application.Options
{
    public class AppSettingsOptions
    {
        public const string AppSettings = "AppSettings";

        public string Secret { get; set; } = string.Empty;
    }
}
