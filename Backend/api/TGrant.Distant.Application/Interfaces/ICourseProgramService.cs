﻿using TGrant.Distant.Contracts.Admins;

namespace TGrant.Distant.Application.Interfaces
{
    public interface ICourseProgramService
    {
        Task<IEnumerable<CourseProgramInfo>> GetAllAsync(int? teacherId = default);
        Task<IEnumerable<CourseInfo>> GetByCourseIdAsync(int courseId);
        Task<CourseProgramInfo> GetByIdAsync(int id, int? teacherId = default);
        Task<int> CreateAsync(CourseProgramDto request);
        Task UpdateAsync(CourseProgramDto request);
        Task<bool> DeleteAsync(int id);
    }
}
