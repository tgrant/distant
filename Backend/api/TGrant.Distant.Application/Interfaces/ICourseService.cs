﻿using TGrant.Distant.Contracts.Admins;

namespace TGrant.Distant.Application.Interfaces
{
    public interface ICourseService
    {
        Task<IEnumerable<CourseInfo>> GetAllAsync();
        Task<CourseInfo> GetByIdAsync(int id);
        Task<IEnumerable<CourseInfo>> GetByBranchIdAsync(int branchId);
        Task<int> CreateAsync(CourseDto request);
        Task UpdateAsync(CourseDto request);
        Task<bool> DeleteAsync(int id);
    }
}
