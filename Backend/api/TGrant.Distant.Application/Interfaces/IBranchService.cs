﻿using TGrant.Distant.Contracts.Admins;

namespace TGrant.Distant.Application.Interfaces
{
    public interface IBranchService
    {
        Task<IEnumerable<BranchInfo>> GetAllAsync();
        Task<BranchInfo> GetByIdAsync(int id);
        Task<int> CreateAsync(BranchDto request);
        Task UpdateAsync(BranchDto request);
        Task<bool> DeleteAsync(int id);
    }
}
