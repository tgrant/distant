﻿using Microsoft.EntityFrameworkCore;
using TGrant.Distant.Core.Entities;

namespace TGrant.Distant.Application.Interfaces
{
    public interface IApplicationDbContext
    {
        DbSet<Branch> Branch { get; set; }
        DbSet<Course> Course { get; set; }
        DbSet<CourseProgram> CourseProgram { get; set; }
        DbSet<Discipline> Discipline { get; set; }
        DbSet<Lesson> Lesson { get; set; }
        DbSet<LessonMaterial> LessonMaterial { get; set; }
        DbSet<Taker> Taker { get; set; }
        DbSet<TakerGroup> TakerGroup { get; set; }
        DbSet<User> User { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken());
    }
}