﻿using TGrant.Distant.Contracts.Authentications;

namespace TGrant.Distant.Application.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<UserInfo>> GetAllAsync();
        Task<UserInfo> GetAsync(string username, string password);
        Task<int> CreateAsync(UserDto userDto);
    }
}