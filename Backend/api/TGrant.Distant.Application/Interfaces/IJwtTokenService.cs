﻿using TGrant.Distant.Core.Entities;

namespace TGrant.Distant.Application.Interfaces
{
    public interface IJwtTokenService
    {
        string GenerateToken(User user);
    }
}