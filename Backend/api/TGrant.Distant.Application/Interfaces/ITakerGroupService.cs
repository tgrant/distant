﻿using TGrant.Distant.Contracts.Admins;

namespace TGrant.Distant.Application.Interfaces
{
    public interface ITakerGroupService
    {
        Task<IEnumerable<TakerGroupInfo>> GetAllAsync(int? teacherId = default);
        Task<IEnumerable<TakerGroupInfo>> GetAllByCourseProgramIdAsync(int courseProgramId, int? teacherId = default);
        Task<TakerGroupInfo> GetByIdAsync(int id, int? teacherId = default);
        Task UpdateAsync(TakerGroupDto request, int? teacherId = default);
        Task<int> CreateAsync(TakerGroupDto request, int? teacherId = default);
        Task<bool> DeleteAsync(int id, int? teacherId = default);
    }
}