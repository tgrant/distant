﻿using TGrant.Distant.Contracts.Admins;

namespace TGrant.Distant.Application.Interfaces
{
    public interface ILessonMaterialService
    {
        Task<IEnumerable<LessonMaterialInfo>> GetAllAsync();
        Task<IEnumerable<LessonMaterialInfo>> GetByLessonIdAsync(int lessonId, int? teacherId = default);
        Task<LessonMaterialInfo> GetByIdAsync(int id, int? teacherId = default);
        Task<int> CreateAsync(LessonMaterialDto request);
        Task UpdateAsync(LessonMaterialDto request);
        Task<bool> DeleteAsync(int id);
    }
}