﻿using TGrant.Distant.Contracts.Admins;

namespace TGrant.Distant.Application.Interfaces
{
    public interface IDisciplineService
    {
        Task<IEnumerable<DisciplineInfo>> GetAllAsync();
        Task<IEnumerable<DisciplineInfo>> GetByCourseProgramIdAsync(int courseProgramId);
        Task<DisciplineInfo> GetByIdAsync(int id);
        Task<int> CreateAsync(DisciplineDto request);
        Task UpdateAsync(DisciplineDto request);
        Task<bool> DeleteAsync(int id);
    }
}
