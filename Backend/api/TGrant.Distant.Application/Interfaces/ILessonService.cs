﻿using TGrant.Distant.Contracts.Admins;

namespace TGrant.Distant.Application.Interfaces
{
    public interface ILessonService
    {
        Task<IEnumerable<LessonInfo>> GetAllAsync();
        Task<IEnumerable<LessonInfo>> GetByDisciplineIdAsync(int disciplineId);
        Task<IEnumerable<LessonInfo>> GetByCourseProgramIdAsync(int courseProgramId, int? teacherId = default);
        Task<LessonInfo> GetByIdAsync(int id, int? teacherId = default);
        Task<int> CreateAsync(LessonDto request);
        Task UpdateAsync(LessonDto request);
        Task<bool> DeleteAsync(int id);
    }
}
