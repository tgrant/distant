﻿using TGrant.Distant.Contracts.Admins;

namespace TGrant.Distant.Application.Interfaces
{
    public interface ITakerService
    {
        Task<IEnumerable<TakerInfo>> GetAllByTakerGroupIdAsync(int takerGroupId, int? teacherId = default);
        Task<TakerInfo> GetByIdAsync(int takerId, int? teacherId = default);
        Task UpdateAsync(TakerDto request, int? teacherId = default);
        Task<int> CreateAsync(TakerDto request, int? teacherId = default);
        Task<bool> DeleteAsync(int id, int? teacherId = default);
    }
}