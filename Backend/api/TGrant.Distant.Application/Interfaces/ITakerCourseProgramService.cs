﻿using TGrant.Distant.Contracts.Admins;

namespace TGrant.Distant.Application.Interfaces
{
    public interface ITakerCourseProgramService
    {
        Task<IEnumerable<TakerCourseProgramInfo>> GetAllAsync(int? userId);
        Task<TakerCourseProgramInfo> GetByIdAsync(int id, int? userId);
        Task<IEnumerable<LessonInfo>> GetLessonsByCourseProgramIdAsync(int id, int? userId);
        Task<IEnumerable<LessonMaterialInfo>> GetLessonMaterialsByLessonIdAsync(int id, int? userId);
    }
}