﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Entities;

namespace TGrant.Distant.Application.Services
{
    public class TakerService : ITakerService
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public TakerService(IApplicationDbContext applicationDbContext, IMapper mapper)
        {
            _context = applicationDbContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TakerInfo>> GetAllByTakerGroupIdAsync(int takerGroupId, int? teacherId)
        {
            var entities = await _context.Taker
                .AsNoTracking()
                .Include(x => x.User)
                .Include(x => x.TakerGroup)
                    .ThenInclude(x => x.CourseProgram)
                .Where(x => x.Id == takerGroupId && (teacherId == null || x.TakerGroup.CourseProgram.UserId == teacherId))
                .ToListAsync();

            return _mapper.Map<List<TakerInfo>>(entities);
        }

        public async Task<TakerInfo> GetByIdAsync(int id, int? teacherId)
        {
            var entity = await _context.Taker
                .AsNoTracking()
                .Include(x => x.User)
                .Include(x => x.TakerGroup)
                    .ThenInclude(x => x.CourseProgram)
                .SingleOrDefaultAsync(x => x.Id == id && (teacherId == null || x.TakerGroup.CourseProgram.UserId == teacherId));

            if (entity is null)
                throw new ApplicationException("entity not found");

            return _mapper.Map<TakerInfo>(entity);
        }

        public async Task UpdateAsync(TakerDto request, int? teacherId)
        {
            var entity = await _context.Taker.SingleOrDefaultAsync(x => x.Id == request.Id);

            if (entity is null)
                throw new ApplicationException("user not found");

            var takerGroup = await _context.TakerGroup
                .Include(x => x.CourseProgram)
                .SingleOrDefaultAsync(x => x.Id == request.TakerGroupId && (teacherId == null || x.CourseProgram.UserId == teacherId));

            if (takerGroup is null)
                throw new ApplicationException("takerGroup not found");

            entity.Update(takerGroup);
            await _context.SaveChangesAsync();
        }

        public async Task<int> CreateAsync(TakerDto request, int? teacherId)
        {
            var takerGroup = await _context.TakerGroup
                .Include(x => x.CourseProgram)
                .SingleOrDefaultAsync(x => x.Id == request.TakerGroupId && (teacherId == null || x.CourseProgram.UserId == teacherId));

            if (takerGroup is null)
                throw new ApplicationException("takerGroup not found");

            var user = await _context.User.SingleOrDefaultAsync(x => x.Id == request.UserId);

            if (user is null)
                throw new ApplicationException("user not found");

            var taker = new Taker(user, takerGroup);

            _context.Taker.Add(taker);
            await _context.SaveChangesAsync();

            return taker.Id;
        }

        public async Task<bool> DeleteAsync(int id, int? teacherId)
        {
            var entity = await _context.Taker
                .Include(x => x.TakerGroup)
                    .ThenInclude(x => x.CourseProgram)
                .Where(x => x.Id == id && (teacherId == null || x.TakerGroup.CourseProgram.UserId == teacherId))
                .SingleOrDefaultAsync(x => x.Id == id);

            if (entity is null) return false;

            _context.Taker.Remove(entity);
            await _context.SaveChangesAsync();

            return true;
        }
    }
}
