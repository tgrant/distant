﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Entities;

namespace TGrant.Distant.Application.Services
{
    public class TakerGroupService : ITakerGroupService
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public TakerGroupService(IApplicationDbContext applicationDbContext, IMapper mapper)
        {
            _context = applicationDbContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TakerGroupInfo>> GetAllAsync(int? teacherId)
        {
            var entities = await _context.TakerGroup
                .AsNoTracking()
                .Include(x => x.CourseProgram)
                .Where(x => teacherId == null || x.CourseProgram.UserId == teacherId)
                .ToListAsync();

            return _mapper.Map<List<TakerGroupInfo>>(entities);
        }

        public async Task<IEnumerable<TakerGroupInfo>> GetAllByCourseProgramIdAsync(int courseProgramId, int? teacherId)
        {
            var entities = await _context.TakerGroup
                .AsNoTracking()
                .Include(x => x.CourseProgram)
                .Where(x => x.CourseProgram.Id == courseProgramId && (teacherId == null || x.CourseProgram.UserId == teacherId))
                .ToListAsync();

            return _mapper.Map<List<TakerGroupInfo>>(entities);
        }

        public async Task<TakerGroupInfo> GetByIdAsync(int id, int? teacherId)
        {
            var entity = await _context.TakerGroup
                .AsNoTracking()
                .Include(x => x.CourseProgram)
                .SingleOrDefaultAsync(x => x.Id == id && (teacherId == null || x.CourseProgram.UserId == teacherId));

            if (entity is null)
                throw new ApplicationException("entity not found");

            return _mapper.Map<TakerGroupInfo>(entity);
        }

        public async Task UpdateAsync(TakerGroupDto request, int? teacherId)
        {
            var entity = await _context.TakerGroup
                .Include(x => x.CourseProgram)
                .SingleOrDefaultAsync(x => x.Id == request.Id && (teacherId == null || x.CourseProgram.UserId == teacherId));

            if (entity is null)
                throw new ApplicationException("entity not found");

            entity.Update(request.Name, request.IsActive);
            await _context.SaveChangesAsync();
        }

        public async Task<int> CreateAsync(TakerGroupDto request, int? teacherId)
        {
            var courseProgram = await _context.CourseProgram
                .SingleOrDefaultAsync(x => x.Id == request.CourseProgramId && (teacherId == null || x.UserId == teacherId));

            if (courseProgram is null)
                throw new ApplicationException("courseProgram not found");

            var takerGroup = await _context.TakerGroup
                .Include(x => x.CourseProgram)
                .SingleOrDefaultAsync(x => x.Name == request.Name && x.CourseProgram.Id == request.CourseProgramId);

            if (takerGroup is not null)
                throw new ApplicationException("takerGroup already exists");

            takerGroup = new TakerGroup(courseProgram, request.Name, request.IsActive);

            _context.TakerGroup.Add(takerGroup);
            await _context.SaveChangesAsync();

            return takerGroup.Id;
        }

        public async Task<bool> DeleteAsync(int id, int? teacherId)
        {
            var entity = await _context.TakerGroup
                .Include(x => x.CourseProgram)
                .SingleOrDefaultAsync(x => x.Id == id && (teacherId == null || x.CourseProgram.UserId == teacherId));

            if (entity is null) return false;

            _context.TakerGroup.Remove(entity);
            await _context.SaveChangesAsync();

            return true;
        }
    }
}
