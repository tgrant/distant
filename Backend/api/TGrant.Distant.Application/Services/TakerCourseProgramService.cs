﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;

namespace TGrant.Distant.Application.Services
{
    public class TakerCourseProgramService : ITakerCourseProgramService
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public TakerCourseProgramService(IApplicationDbContext applicationDbContext, IMapper mapper)
        {
            _context = applicationDbContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TakerCourseProgramInfo>> GetAllAsync(int? userId)
        {
            var entities = await _context.Taker
                .AsNoTracking()
                .Include(x => x.User)
                .Include(x => x.TakerGroup)
                    .ThenInclude(x => x.CourseProgram)
                .Where(x => userId == null || x.User.Id == userId)
                .ToListAsync();

            return _mapper.Map<List<TakerCourseProgramInfo>>(entities);
        }

        public async Task<TakerCourseProgramInfo> GetByIdAsync(int id, int? userId)
        {
            var courseProgram = await _context.Taker
                .AsNoTracking()
                .Include(x => x.User)
                .Include(x => x.TakerGroup)
                    .ThenInclude(x => x.CourseProgram)
                .FirstOrDefaultAsync(x => x.TakerGroup.CourseProgram.Id == id && x.User.Id == userId);

            if (courseProgram is null)
                throw new ApplicationException("CourseProgram not found");

            var entity = await _context.CourseProgram
                .AsNoTracking()
                .Include(x => x.Course)
                .SingleOrDefaultAsync(x => x.Id == id);

            return _mapper.Map<TakerCourseProgramInfo>(entity);
        }

        public async Task<IEnumerable<LessonInfo>> GetLessonsByCourseProgramIdAsync(int id, int? userId)
        {
            var courseProgram = await _context.Taker
                .AsNoTracking()
                .Include(x => x.User)
                .Include(x => x.TakerGroup)
                    .ThenInclude(x => x.CourseProgram)
                .FirstOrDefaultAsync(x => x.TakerGroup.CourseProgram.Id == id && x.User.Id == userId);

            if (courseProgram is null)
                throw new ApplicationException("CourseProgram not found");

            var lessons = await _context.Lesson
                .AsNoTracking()
                .Include(x => x.Discipline)
                    .ThenInclude(x => x.CourseProgram)
                .Where(x => x.Discipline.CourseProgram.Id == id)
                .ToListAsync();

            return _mapper.Map<IEnumerable<LessonInfo>>(lessons);
        }

        public async Task<IEnumerable<LessonMaterialInfo>> GetLessonMaterialsByLessonIdAsync(int id, int? userId)
        {
            var lesson = await _context.Lesson
                .AsNoTracking()
                    .Include(x => x.Discipline)
                        .ThenInclude(x => x.CourseProgram)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (lesson is null)
                throw new ApplicationException("Lesson not found");

            var taker = await _context.Taker
                .AsNoTracking()
                .Include(x => x.TakerGroup)
                    .ThenInclude(x => x.CourseProgram)
                .FirstOrDefaultAsync(x => x.TakerGroup.CourseProgram.Id == lesson.Discipline.CourseProgram.Id && x.User.Id == userId);

            if (taker is null)
                throw new ApplicationException("Lesson for taker not found");

            var lessonMaterials = await _context.LessonMaterial
                .AsNoTracking()
                .Include(x => x.Lesson)
                    .ThenInclude(x => x.Discipline)
                       .ThenInclude(x => x.CourseProgram)
                .Where(x => x.Lesson.Id == id)
                .ToListAsync();

            return _mapper.Map<IEnumerable<LessonMaterialInfo>>(lessonMaterials);
        }
    }
}
