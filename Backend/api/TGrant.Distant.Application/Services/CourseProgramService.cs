﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Entities;

namespace TGrant.Distant.Application.Services
{
    public class CourseProgramService : ICourseProgramService
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public CourseProgramService(IApplicationDbContext applicationDbContext, IMapper mapper)
        {
            _context = applicationDbContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<CourseProgramInfo>> GetAllAsync(int? teacherId = default)
        {
            var entities = await _context.CourseProgram
                .AsNoTracking()
                .Where(x => teacherId == null || x.UserId == teacherId)
                .ToListAsync();

            return _mapper.Map<List<CourseProgramInfo>>(entities);
        }

        public async Task<IEnumerable<CourseInfo>> GetByCourseIdAsync(int courseId)
        {
            var entities = await _context.CourseProgram
                .Include(x => x.Course)
                .Where(x => x.Course.Id == courseId)
                .ToListAsync();

            return _mapper.Map<List<CourseInfo>>(entities);
        }

        public async Task<CourseProgramInfo> GetByIdAsync(int id, int? teacherId = default)
        {
            var entity = await _context.CourseProgram
                .AsNoTracking()
                .SingleOrDefaultAsync(x => x.Id == id && (teacherId == null || x.UserId == teacherId));

            if (entity is null)
                throw new ApplicationException("entity not found");

            return _mapper.Map<CourseProgramInfo>(entity);
        }

        public async Task UpdateAsync(CourseProgramDto request)
        {
            var entity = await _context.CourseProgram.SingleOrDefaultAsync(x => x.Id == request.Id);

            if (entity is null)
                throw new ApplicationException("entity not found");

            var user = await _context.User.SingleOrDefaultAsync(x => x.Id == request.UserId);

            entity.Update(request.Name, request.Description, user?.Id, request.IsActive);
            await _context.SaveChangesAsync();
        }

        public async Task<int> CreateAsync(CourseProgramDto request)
        {
            var entity = await _context.Course.SingleOrDefaultAsync(x => x.Id == request.CourseId);

            if (entity is null)
                throw new ApplicationException("entity not found");

            var user = await _context.User.SingleOrDefaultAsync(x => x.Id == request.UserId);

            var item = new CourseProgram(request.Name, request.Description, user?.Id, request.IsActive);

            entity.CoursePrograms.Add(item);
            await _context.SaveChangesAsync();

            return item.Id;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var entity = await _context.CourseProgram.SingleOrDefaultAsync(x => x.Id == id);

            if (entity is null) return false;

            _context.CourseProgram.Remove(entity);
            await _context.SaveChangesAsync();

            return true;
        }
    }
}
