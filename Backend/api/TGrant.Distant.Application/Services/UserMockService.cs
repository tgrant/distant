﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Authentications;

namespace TGrant.Distant.Application.Services
{
    public class UserMockService : UserService, IUserService
    {
        private readonly IApplicationDbContext _context;
        private readonly IJwtTokenService _jwtTokenService;
        private readonly IMapper _mapper;

        public UserMockService(IApplicationDbContext context, IMapper mapper, IJwtTokenService jwtTokenService)
            : base(context, mapper, jwtTokenService)
        {
            _context = context;
            _mapper = mapper;
            _jwtTokenService = jwtTokenService;
        }

        public async override Task<UserInfo> GetAsync(string username, string password)
        {
            var user = await _context.User.SingleOrDefaultAsync(x => x.Username == username);

            if (user is null)
                throw new ApplicationException("Username or password is incorrect");

            var userInfo = _mapper.Map<UserInfo>(user);

            userInfo.Token = _jwtTokenService.GenerateToken(user);

            return userInfo;
        }
    }
}
