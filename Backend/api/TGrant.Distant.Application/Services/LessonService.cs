﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Entities;

namespace TGrant.Distant.Application.Services
{
    public class LessonService : ILessonService
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public LessonService(IApplicationDbContext applicationDbContext, IMapper mapper)
        {
            _context = applicationDbContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<LessonInfo>> GetAllAsync()
        {
            var items = await _context.Lesson.AsNoTracking().ToListAsync();
            return _mapper.Map<List<LessonInfo>>(items);
        }

        public async Task<IEnumerable<LessonInfo>> GetByDisciplineIdAsync(int disciplineId)
        {
            var entities = await _context.Lesson
                .AsNoTracking()
                .Include(x => x.Discipline)
                .Where(x => x.Discipline.Id == disciplineId)
                .ToListAsync();

            return _mapper.Map<List<LessonInfo>>(entities);
        }

        public async Task<IEnumerable<LessonInfo>> GetByCourseProgramIdAsync(int courseProgramId, int? teacherId)
        {
            var entities = await _context.Lesson
                .AsNoTracking()
                .Include(x => x.Discipline)
                    .ThenInclude(x => x.CourseProgram)
                .Where(x => x.Discipline.CourseProgram.Id == courseProgramId && (teacherId == null || x.Discipline.CourseProgram.UserId == teacherId))
                .ToListAsync();

            return _mapper.Map<List<LessonInfo>>(entities);
        }

        public async Task<LessonInfo> GetByIdAsync(int id, int? teacherId = default)
        {
            var item = await _context.Lesson
                .AsNoTracking()
                .SingleOrDefaultAsync(x => x.Id == id && (teacherId == null || x.Discipline.CourseProgram.UserId == teacherId));

            if (item is null)
                throw new ApplicationException("item not found");

            return _mapper.Map<LessonInfo>(item);
        }

        public async Task UpdateAsync(LessonDto request)
        {
            var entity = await _context.Lesson.SingleOrDefaultAsync(x => x.Id == request.Id);

            if (entity is null)
                throw new ApplicationException("entity not found");

            entity.Update(request.Name, request.Description);
            await _context.SaveChangesAsync();
        }

        public async Task<int> CreateAsync(LessonDto request)
        {
            var entity = await _context.Discipline.SingleOrDefaultAsync(x => x.Id == request.DisciplineId);

            if (entity is null)
                throw new ApplicationException("entity not found");

            var item = new Lesson(request.Name, request.Description);

            entity.Lessons.Add(item);
            await _context.SaveChangesAsync();

            return entity.Id;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var entity = await _context.Lesson.SingleOrDefaultAsync(x => x.Id == id);

            if (entity is null) return false;

            _context.Lesson.Remove(entity);
            await _context.SaveChangesAsync();

            return true;
        }
    }
}
