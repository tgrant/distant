﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Entities;

namespace TGrant.Distant.Application.Services
{
    public class LessonMaterialService : ILessonMaterialService
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public LessonMaterialService(IApplicationDbContext applicationDbContext, IMapper mapper)
        {
            _context = applicationDbContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<LessonMaterialInfo>> GetAllAsync()
        {
            var items = await _context.LessonMaterial.AsNoTracking().ToListAsync();
            return _mapper.Map<List<LessonMaterialInfo>>(items);
        }

        public async Task<IEnumerable<LessonMaterialInfo>> GetByLessonIdAsync(int lessonId, int? teacherId = default)
        {
            var lesson = await _context.Lesson
                .AsNoTracking()
                .Include(x => x.LessonMaterials)
                .Include(x => x.Discipline)
                    .ThenInclude(x => x.CourseProgram)
                .SingleOrDefaultAsync(x => x.Id == lessonId && (teacherId == null || x.Discipline.CourseProgram.UserId == teacherId));
            
            if (lesson is null)
                throw new ApplicationException("lesson not found");

            return _mapper.Map<List<LessonMaterialInfo>>(lesson.LessonMaterials);
        }

        public async Task<LessonMaterialInfo> GetByIdAsync(int id, int? teacherId = default)
        {
            var item = await _context.LessonMaterial
                .AsNoTracking()
                 .Include(x => x.Lesson)
                    .ThenInclude(x => x.Discipline)
                        .ThenInclude(x => x.CourseProgram)
                .SingleOrDefaultAsync(x => x.Id == id && (teacherId == null || x.Lesson.Discipline.CourseProgram.UserId == teacherId));

            if (item is null)
                throw new ApplicationException("item not found");

            return _mapper.Map<LessonMaterialInfo>(item);
        }

        public async Task UpdateAsync(LessonMaterialDto request)
        {
            var entity = await _context.LessonMaterial.SingleOrDefaultAsync(x => x.Id == request.Id);

            if (entity is null)
                throw new ApplicationException("entity not found");

            entity.Update(request.Type, request.Name, request.Link);
            await _context.SaveChangesAsync();
        }

        public async Task<int> CreateAsync(LessonMaterialDto request)
        {
            var entity = await _context.Lesson.SingleOrDefaultAsync(x => x.Id == request.LessonId);

            if (entity is null)
                throw new ApplicationException("entity not found");

            var item = new LessonMaterial(request.Type, request.Name, request.Link);

            entity.LessonMaterials.Add(item);
            await _context.SaveChangesAsync();

            return entity.Id;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var entity = await _context.LessonMaterial.SingleOrDefaultAsync(x => x.Id == id);

            if (entity is null) return false;

            _context.LessonMaterial.Remove(entity);
            await _context.SaveChangesAsync();

            return true;
        }
    }
}
