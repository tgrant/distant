﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Entities;

namespace TGrant.Distant.Application.Services
{
    public class BranchService : IBranchService
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public BranchService(IApplicationDbContext applicationDbContext, IMapper mapper)
        {
            _context = applicationDbContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<BranchInfo>> GetAllAsync()
        {
            var entities = await _context.Branch.Include(x => x.Courses).AsNoTracking().ToListAsync();
            return _mapper.Map<List<BranchInfo>>(entities);
        }

        public async Task<BranchInfo> GetByIdAsync(int id)
        {
            var entity = await _context.Branch.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);

            if (entity is null)
                throw new ApplicationException("entity not found");

            return _mapper.Map<BranchInfo>(entity);
        }

        public async Task UpdateAsync(BranchDto request)
        {
            var entity = await _context.Branch.SingleOrDefaultAsync(x => x.Id == request.Id);

            if (entity is null)
                throw new ApplicationException("entity not found");

            entity.Update(request.Name, request.Description, request.IsActive);

            await _context.SaveChangesAsync();
        }

        public async Task<int> CreateAsync(BranchDto request)
        {
            var entity = await _context.Branch.SingleOrDefaultAsync(x => x.Name == request.Name);

            if (entity is not null)
                throw new ApplicationException("entity with such name already exists");

            entity = new Branch(request.Name, request.Description, request.IsActive);

            await _context.Branch.AddAsync(entity);
            await _context.SaveChangesAsync();

            return entity.Id;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var entity = await _context.Branch.SingleOrDefaultAsync(x => x.Id == id);

            if (entity is null) return false;

            _context.Branch.Remove(entity);
            await _context.SaveChangesAsync();

            return true;
        }
    }
}
