﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Entities;

namespace TGrant.Distant.Application.Services
{
    public class CourseService : ICourseService
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public CourseService(IApplicationDbContext applicationDbContext, IMapper mapper)
        {
            _context = applicationDbContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<CourseInfo>> GetAllAsync()
        {
            var entities = await _context.Course.Include(x => x.Branch).AsNoTracking().ToListAsync();
            return _mapper.Map<List<CourseInfo>>(entities);
        }

        public async Task<IEnumerable<CourseInfo>> GetByBranchIdAsync(int branchId)
        {
            var entities = await _context.Course
                .Include(x => x.Branch)
                .Where(x=> x.Branch.Id == branchId)
                .ToListAsync();

            return _mapper.Map< List<CourseInfo>>(entities);
        }

        public async Task<CourseInfo> GetByIdAsync(int id)
        {
            var entity = await _context.Course.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);

            if (entity is null)
                throw new ApplicationException("entity not found");

            return _mapper.Map<CourseInfo>(entity);
        }

        public async Task UpdateAsync(CourseDto request)
        {
            var entity = await _context.Course.SingleOrDefaultAsync(x => x.Id == request.Id);

            if (entity is null)
                throw new ApplicationException("entity not found");

            entity.Update(request.Name, request.Description, request.IsActive);
            await _context.SaveChangesAsync();
        }

        public async Task<int> CreateAsync(CourseDto request)
        {
            var entity = await _context.Branch.SingleOrDefaultAsync(x => x.Id == request.BranchId);

            if (entity is null)
                throw new ApplicationException("entity not found");

            var item = new Course(request.Name, request.Description, request.IsActive);

            entity.Courses.Add(item);
            await _context.SaveChangesAsync();

            return entity.Id;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var entity = await _context.Course.SingleOrDefaultAsync(x => x.Id == id);

            if (entity is null) return false;

            _context.Course.Remove(entity);
            await _context.SaveChangesAsync();

            return true;
        }
    }
}
