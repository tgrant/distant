﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Entities;

namespace TGrant.Distant.Application.Services
{
    public class DisciplineService : IDisciplineService
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public DisciplineService(IApplicationDbContext applicationDbContext, IMapper mapper)
        {
            _context = applicationDbContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<DisciplineInfo>> GetAllAsync()
        {
            var subjects = await _context.Discipline.AsNoTracking().ToListAsync();
            return _mapper.Map<List<DisciplineInfo>>(subjects);
        }

        public async Task<IEnumerable<DisciplineInfo>> GetByCourseProgramIdAsync(int courseProgramId)
        {
            var entities = await _context.Discipline
                .Include(x => x.CourseProgram)
                .Where(x => x.CourseProgram.Id == courseProgramId)
                .ToListAsync();

            return _mapper.Map<List<DisciplineInfo>>(entities);
        }

        public async Task<DisciplineInfo> GetByIdAsync(int id)
        {
            var item = await _context.Discipline.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);

            if (item is null)
                throw new ApplicationException("item not found");

            return _mapper.Map<DisciplineInfo>(item);
        }

        public async Task UpdateAsync(DisciplineDto request)
        {
            var entity = await _context.Discipline.SingleOrDefaultAsync(x => x.Id == request.Id);

            if (entity is null)
                throw new ApplicationException("entity not found");

            entity.Update(request.Name, request.Description);
            await _context.SaveChangesAsync();
        }

        public async Task<int> CreateAsync(DisciplineDto request)
        {
            var entity = await _context.CourseProgram.SingleOrDefaultAsync(x => x.Id == request.CourseProgramId);

            if (entity is null)
                throw new ApplicationException("entity not found");

            var item = new Discipline(request.Name, request.Description);

            entity.Disciplines.Add(item);
            await _context.SaveChangesAsync();

            return entity.Id;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var entity = await _context.Discipline.SingleOrDefaultAsync(x => x.Id == id);

            if (entity is null) return false;

            _context.Discipline.Remove(entity);
            await _context.SaveChangesAsync();

            return true;
        }
    }
}
