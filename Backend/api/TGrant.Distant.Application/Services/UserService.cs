﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Authentications;
using TGrant.Distant.Core.Entities;

namespace TGrant.Distant.Application.Services
{
    public class UserService : IUserService
    {
        private readonly IApplicationDbContext _context;
        private readonly IJwtTokenService _jwtTokenService;
        private readonly IMapper _mapper;

        public UserService(IApplicationDbContext context, IMapper mapper, IJwtTokenService jwtTokenService)
        {
            _context = context;
            _mapper = mapper;
            _jwtTokenService = jwtTokenService;
        }

        public async Task<IEnumerable<UserInfo>> GetAllAsync()
        {
            var users = _context.User;
            return _mapper.Map<List<UserInfo>>(users);
        }

        public async virtual Task<UserInfo> GetAsync(string username, string password)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.Username == username && x.Password == password);

            if (user is null)
                throw new ApplicationException("Username or password is incorrect");

            var userInfo = _mapper.Map<UserInfo>(user);

            userInfo.Token = _jwtTokenService.GenerateToken(user);

            return userInfo;
        }

        public async Task<int> CreateAsync(UserDto userDto)
        {
            var user = await _context.User.SingleOrDefaultAsync(x => x.Username == userDto.Username);

            if (user is not null)
                throw new ApplicationException("User with such username already exists");

            user = new User(userDto.FirstName, userDto.LastName, userDto.Username, userDto.Password, userDto.Role);

            _context.User.Add(user);
            await _context.SaveChangesAsync();

            return user.Id;
        }
    }
}
