﻿using AutoMapper;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Contracts.Authentications;
using TGrant.Distant.Core.Entities;

namespace TGrant.Distant.Application.MapperProfiles
{
    public class ModelsProfile : Profile
    {
        public ModelsProfile()
        {
            CreateMap<User, UserInfo>();

            CreateMap<Branch, BranchInfo>();
            CreateMap<Course, CourseInfo>();
            CreateMap<CourseProgram, CourseProgramInfo>();
            CreateMap<Discipline, DisciplineInfo>();
            CreateMap<Lesson, LessonInfo>();
            CreateMap<LessonMaterial, LessonMaterialInfo>();
            CreateMap<User, UserInfo>();

            CreateMap<Taker, TakerInfo>();
            CreateMap<TakerGroup, TakerGroupInfo>();

            CreateMap<Taker, TakerCourseProgramInfo>()
                .ForPath(x => x.Id, x => x.MapFrom(x => x.TakerGroup.CourseProgram.Id))
                .ForPath(x => x.Name, x => x.MapFrom(x => x.TakerGroup.CourseProgram.Name))
                .ForPath(x => x.Name, x => x.MapFrom(x => x.TakerGroup.CourseProgram.Description));

            CreateMap<CourseProgram, TakerCourseProgramInfo>()
                .ForPath(x => x.CourseId, x => x.MapFrom(x => x.Course.Id))
                .ForPath(x => x.CourseName, x => x.MapFrom(x => x.Course.Name));

            CreateMap<Lesson, LessonInfo>()
                .ForPath(x => x.DisciplineId, x => x.MapFrom(x => x.Discipline.Id))
                .ForPath(x => x.DisciplineName, x => x.MapFrom(x => x.Discipline.Name))
                .ForPath(x => x.CourseProgramId, x => x.MapFrom(x => x.Discipline.CourseProgram.Id))
                .ForPath(x => x.CourseProgramName, x => x.MapFrom(x => x.Discipline.CourseProgram.Name));

            CreateMap<LessonMaterial, LessonMaterialInfo>()
                .ForPath(x => x.LessonId, x => x.MapFrom(x => x.Lesson.Id))
                .ForPath(x => x.LessonName, x => x.MapFrom(x => x.Lesson.Name));
        }
    }
}
