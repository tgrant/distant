﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using TGrant.Distant.Application.Authentications;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Application.Services;

namespace TGrant.Distant.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services, bool isMockData)
        {
            var assembly = Assembly.GetExecutingAssembly().GetName().Name;

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddMaps(assembly);
            });
            var configuration = new MapperConfiguration(cfg => cfg.AddMaps(assembly));
            IMapper mapper = configuration.CreateMapper();
            services.AddSingleton(mapper);

            services.AddTransient<IJwtTokenService, JwtTokenService>();
            services.AddTransient<IBranchService, BranchService>();
            services.AddTransient<ICourseService, CourseService>();
            services.AddTransient<ICourseProgramService, CourseProgramService>();
            services.AddTransient<IDisciplineService, DisciplineService>();
            services.AddTransient<ILessonService, LessonService>();
            services.AddTransient<ILessonMaterialService, LessonMaterialService>();

            services.AddTransient<ITakerGroupService, TakerGroupService>();
            services.AddTransient<ITakerService, TakerService>();
            services.AddTransient<ITakerCourseProgramService, TakerCourseProgramService>();

            if (isMockData)
                services.AddTransient<IUserService, UserMockService>();
            else
                services.AddTransient<IUserService, UserService>();

            return services;
        }
    }
}
