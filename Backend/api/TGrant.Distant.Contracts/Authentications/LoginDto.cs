﻿using System.ComponentModel.DataAnnotations;

namespace TGrant.Distant.Contracts.Authentications
{
    public class LoginDto
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
