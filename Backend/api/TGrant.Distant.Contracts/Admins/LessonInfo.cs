﻿namespace TGrant.Distant.Contracts.Admins
{
    public class LessonInfo
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int DisciplineId { get; set; }

        public string DisciplineName { get; set; }

        public int CourseProgramId { get; set; }

        public string CourseProgramName { get; set; }
    }
}
