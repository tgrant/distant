﻿namespace TGrant.Distant.Contracts.Admins
{
    public class LessonDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int DisciplineId { get; set; }
    }
}
