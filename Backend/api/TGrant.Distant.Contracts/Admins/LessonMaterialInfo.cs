﻿namespace TGrant.Distant.Contracts.Admins
{
    public class LessonMaterialInfo
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public string Link { get; set; }

        public int LessonId { get; set; }

        public string LessonName { get; set; }
    }
}
