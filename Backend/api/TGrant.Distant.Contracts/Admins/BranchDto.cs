﻿namespace TGrant.Distant.Contracts.Admins
{
    public class BranchDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }
    }
}
