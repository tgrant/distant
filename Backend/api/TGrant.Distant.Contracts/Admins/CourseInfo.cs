﻿namespace TGrant.Distant.Contracts.Admins
{
    public class CourseInfo
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        public int BranchId { get; set; }

        public string BranchName { get; set; }
    }
}
