﻿namespace TGrant.Distant.Contracts.Admins
{
    public class DisciplineInfo
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int CourseProgramId { get; set; }

        public string CourseProgramName { get; set; }

        public int LessonCount{ get; set; }
    }
}
