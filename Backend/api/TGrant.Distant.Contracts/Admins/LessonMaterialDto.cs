﻿using TGrant.Distant.Core.Enums;

namespace TGrant.Distant.Contracts.Admins
{
    public class LessonMaterialDto
    {
        public int Id { get; set; }

        public LessonMaterialType Type { get; set; }

        public string Name { get; set; }

        public string Link { get; set; }

        public int LessonId { get; set; }
    }
}
