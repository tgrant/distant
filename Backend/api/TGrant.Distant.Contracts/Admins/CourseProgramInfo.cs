﻿namespace TGrant.Distant.Contracts.Admins
{
    public class CourseProgramInfo
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int CourseId { get; set; }

        public string CourseName { get; set; }

        public bool IsActive { get; private set; }

        public int UserId { get; set; }

        public string UserName { get; set; }
    }
}
