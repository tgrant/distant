﻿namespace TGrant.Distant.Contracts.Admins
{
    public class TakerGroupInfo
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsActive { get; set; }

        public int CourseProgramId { get; set; }

        public string CourseProgramName { get; set; }

        public string CourseName { get; set; }
    }
}
