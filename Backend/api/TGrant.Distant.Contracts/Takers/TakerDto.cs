﻿namespace TGrant.Distant.Contracts.Admins
{
    public class TakerDto
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int TakerGroupId { get; set; }
    }
}
