﻿namespace TGrant.Distant.Contracts.Admins
{
    public class TakerInfo
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int TakerGroupId { get; set; }

        public string Username { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string GroupName { get; set; }
    }
}
