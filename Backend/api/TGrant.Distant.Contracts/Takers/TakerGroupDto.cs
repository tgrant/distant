﻿namespace TGrant.Distant.Contracts.Admins
{
    public class TakerGroupDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsActive { get; set; }

        public int CourseProgramId { get; set; }
    }
}
