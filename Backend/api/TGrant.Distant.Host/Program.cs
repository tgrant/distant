using Fedex.Customers.DataAccess;
using Microsoft.IdentityModel.Logging;
using TGrant.Distant.Application;
using TGrant.Distant.Application.Options;
using TGrant.Distant.Host.Extensions;
using TGrant.Distant.Infrastructure;
using TGrant.Distant.Infrastructure.Persistence;

var builder = WebApplication.CreateBuilder(args);

builder.Services.Configure<AppSettingsOptions>(builder.Configuration.GetSection(AppSettingsOptions.AppSettings));

builder.Logging.ClearProviders();
builder.Logging.AddConsole();
// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerWithAuthorization();
builder.Services.AddWebApiAuthentication(builder.Configuration);

builder.Services.AddInfrastructure(builder.Configuration);
builder.Services.AddApplication(builder.Environment.IsDevelopment());

var app = builder.Build();
app.MigrateDatabase<ApplicationDbContext>();

// For test
app.UseSwagger();
app.UseSwaggerUI();

// configure HTTP request pipeline
app.UseCors(x => x
    .AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader());

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    IdentityModelEventSource.ShowPII = true;
}

using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;

    try
    {
        var context = services.GetRequiredService<ApplicationDbContext>();
        await ApplicationDbContextSeed.Seed(context);
    }
    catch (Exception ex)
    {
        throw new Exception("Failed to seed data");
    }
}

app.UseHttpsRedirection();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run("http://0.0.0.0:21041");
