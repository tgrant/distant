﻿using System;
using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Polly;

namespace Fedex.Customers.DataAccess
{
    /// <summary>
    /// Класс для поддержки миграций БД
    /// </summary>
    public static class MigrationExtensions
    {
        /// <summary>
        /// Выполнить требуемые миграции БД
        /// </summary>
        public static IHost MigrateDatabase<TContext>(this IHost host, Action<TContext, IServiceProvider>? seeder = default)
            where TContext : DbContext
        {
            using var scope = host.Services.CreateScope();
            var services = scope.ServiceProvider;
            var logger = services.GetRequiredService<ILogger<TContext>>();
            using var context = services.GetRequiredService<TContext>();
            try
            {
                logger.LogInformation("Migrating database associated with context {DbContextName}", typeof(TContext).Name);
                const int retries = 10;
                var retry = Policy.Handle<DbException>()
                    .WaitAndRetry(
                        retryCount: retries,
                        sleepDurationProvider: retry => TimeSpan.FromSeconds(Math.Pow(2, retry)),
                        onRetry: (ex, timeSpan, retry, ctx) =>
                        {
                            logger.LogWarning(ex, "[{Prefix}] Exception {ExceptionType} with message {Message} detected on attempt {Retry} of {Retries}",
                                nameof(TContext), ex.GetType().Name, ex.Message, retry.ToString(), retries.ToString());
                        });

                // if the db server container is not created on run docker compose this
                // migration can't fail for network related exception. The retry options for DbContext only 
                // apply to transient exceptions
                // Note that this is NOT applied when running some orchestrators (let the orchestrator to recreate the failing service)
                retry.Execute(() => InvokeSeeder(seeder, context, services));

                logger.LogInformation("Migrated database associated with context {DbContextName}", typeof(TContext).Name);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "An error occurred while migrating the database used on context {DbContextName}", typeof(TContext).Name);
                // throw;
            }
            return host;
        }

        private static void InvokeSeeder<TContext>(Action<TContext, IServiceProvider>? seeder, TContext context, IServiceProvider services)
            where TContext : DbContext
        {
            context.Database.Migrate();
            seeder?.Invoke(context, services);
        }
    }
}