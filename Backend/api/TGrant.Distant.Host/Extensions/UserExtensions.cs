﻿using System.Security.Claims;

namespace TGrant.Distant.Host.Extensions
{
    public static class UserExtensions
    {
        public static int? GetIdOrNull(this ClaimsPrincipal user, string role)
        {
            var userIdentityName = user.Identity?.Name;

            if (userIdentityName is null)
                return null;

            return user.IsInRole(role) ? int.Parse(userIdentityName) : null;
        }
    }
}
