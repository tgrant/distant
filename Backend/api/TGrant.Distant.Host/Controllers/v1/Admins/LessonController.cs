﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Common;

namespace TGrant.Distant.Host.Controllers.v1.Admins
{
    /// <summary>
    /// Уроки доступные Админу
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class LessonController : ControllerBase
    {
        private readonly ILessonService _lessonService;

        public LessonController(ILessonService lessonService)
        {
            _lessonService = lessonService;
        }

        /// <summary>
        /// Получить все уроки
        /// </summary>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<LessonInfo>))]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> GetAllAsync()
        {
            var items = await _lessonService.GetAllAsync();
            return Ok(items);
        }

        /// <summary>
        /// Получить уроки по дисциплине
        /// </summary>
        [HttpGet("{disciplineId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<LessonInfo>))]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> GetByDisciplineIdAsync(int disciplineId)
        {
            var items = await _lessonService.GetByDisciplineIdAsync(disciplineId);
            return Ok(items);
        }

        /// <summary>
        /// Получить урок по id
        /// </summary>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(LessonInfo))]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var item = await _lessonService.GetByIdAsync(id);
            return Ok(item);
        }

        /// <summary>
        /// Обновить урок
        /// </summary>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> UpdateAsync(LessonDto request)
        {
            await _lessonService.UpdateAsync(request);
            return Ok();
        }

        /// <summary>
        /// Создать урок
        /// </summary>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(int))]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> CreateAsync(LessonDto request)
        {
            var id = await _lessonService.CreateAsync(request);
            return Ok(id);
        }

        /// <summary>
        /// Удалить урок
        /// </summary>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            if (!await _lessonService.DeleteAsync(id))
                return NoContent();

            return Accepted();
        }
    }
}
