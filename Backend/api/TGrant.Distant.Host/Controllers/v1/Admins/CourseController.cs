﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Common;

namespace TGrant.Distant.Host.Controllers.v1.Admins
{
    /// <summary>
    /// Курсы доступные Админу
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CourseController : ControllerBase
    {
        private readonly ICourseService _courseService;

        public CourseController(ICourseService courseService)
        {
            _courseService = courseService;
        }

        /// <summary>
        /// Получить все курсы
        /// </summary>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<CourseInfo>))]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> GetAllAsync()
        {
            var items = await _courseService.GetAllAsync();
            return Ok(items);
        }

        /// <summary>
        /// Получить курсы по образовательной ветке
        /// </summary>
        [HttpGet("{branchId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CourseInfo))]
        [Authorize(Roles = Role.Admin)]

        public async Task<IActionResult> GetByBranchIdAsync(int branchId)
        {
            var items = await _courseService.GetByBranchIdAsync(branchId);
            return Ok(items);
        }

        /// <summary>
        /// Получить курс по id
        /// </summary>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CourseInfo))]
        [Authorize(Roles = Role.Admin)]

        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var item = await _courseService.GetByIdAsync(id);
            return Ok(item);
        }

        /// <summary>
        /// Обновить курс
        /// </summary>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = Role.Admin)]

        public async Task<IActionResult> UpdateAsync(CourseDto request)
        {
            await _courseService.UpdateAsync(request);
            return Ok();
        }

        /// <summary>
        /// Создать курс
        /// </summary>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(int))]
        [Authorize(Roles = Role.Admin)]

        public async Task<IActionResult> CreateAsync(CourseDto request)
        {
            var id = await _courseService.CreateAsync(request);
            return Ok(id);
        }

        /// <summary>
        /// Удалить курс
        /// </summary>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            if (!await _courseService.DeleteAsync(id))
                return NoContent();

            return Accepted();
        }
    }
}
