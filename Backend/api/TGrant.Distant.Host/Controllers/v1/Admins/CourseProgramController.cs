﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Common;

namespace TGrant.Distant.Host.Controllers.v1.Admins
{
    /// <summary>
    /// Программы курсов доступные Админу
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CourseProgramController : ControllerBase
    {
        private readonly ICourseProgramService _courseProgramService;

        public CourseProgramController(ICourseProgramService courseProgramService)
        {
            _courseProgramService = courseProgramService;
        }

        /// <summary>
        /// Получить все программы курсов
        /// </summary>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<CourseProgramInfo>))]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> GetAllAsync()
        {
            var items = await _courseProgramService.GetAllAsync();
            return Ok(items);
        }

        /// <summary>
        /// Получить программы курсов по курсу
        /// </summary>
        [HttpGet("{courseId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<CourseProgramInfo>))]
        [Authorize(Roles = Role.Admin)]

        public async Task<IActionResult> GetByBranchIdAsync(int courseId)
        {
            var items = await _courseProgramService.GetByCourseIdAsync(courseId);
            return Ok(items);
        }

        /// <summary>
        /// Получить программу курса по id
        /// </summary>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CourseProgramInfo))]
        [Authorize(Roles = Role.Admin)]

        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var item = await _courseProgramService.GetByIdAsync(id);
            return Ok(item);
        }

        /// <summary>
        /// Обновить программу курсау
        /// </summary>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = Role.Admin)]

        public async Task<IActionResult> UpdateAsync(CourseProgramDto request)
        {
            await _courseProgramService.UpdateAsync(request);
            return Ok();
        }

        /// <summary>
        /// Создать программу курса
        /// </summary>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(int))]
        [Authorize(Roles = Role.Admin)]

        public async Task<IActionResult> CreateAsync(CourseProgramDto request)
        {
            var id = await _courseProgramService.CreateAsync(request);
            return Ok(id);
        }

        /// <summary>
        /// Удалить программу курса
        /// </summary>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            if (!await _courseProgramService.DeleteAsync(id))
                return NoContent();

            return Accepted();
        }
    }
}
