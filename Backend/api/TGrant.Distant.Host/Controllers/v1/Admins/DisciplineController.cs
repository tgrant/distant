﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Common;

namespace TGrant.Distant.Host.Controllers.v1.Admins
{
    /// <summary>
    /// Дисциплины доступные Админу
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DisciplineController : ControllerBase
    {
        private readonly IDisciplineService _disciplineService;

        public DisciplineController(IDisciplineService disciplineService)
        {
            _disciplineService = disciplineService;
        }

        /// <summary>
        /// Получить все дисциплины
        /// </summary>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<DisciplineInfo>))]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> GetAllAsync()
        {
            var items = await _disciplineService.GetAllAsync();
            return Ok(items);
        }

        /// <summary>
        /// Получить дисциплины по программе курсов
        /// </summary>
        [HttpGet("{courseProgramId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CourseInfo))]
        [Authorize(Roles = Role.Admin)]

        public async Task<IActionResult> GetByCourseProgramIdAsync(int courseProgramId)
        {
            var items = await _disciplineService.GetByCourseProgramIdAsync(courseProgramId);
            return Ok(items);
        }

        /// <summary>
        /// Получить дисциплину по id
        /// </summary>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DisciplineInfo))]
        [Authorize(Roles = Role.Admin)]

        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var item = await _disciplineService.GetByIdAsync(id);
            return Ok(item);
        }

        /// <summary>
        /// Обновить дисциплину
        /// </summary>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = Role.Admin)]

        public async Task<IActionResult> UpdateAsync(DisciplineDto request)
        {
            await _disciplineService.UpdateAsync(request);
            return Ok();
        }

        /// <summary>
        /// Создать дисциплину
        /// </summary>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(int))]
        [Authorize(Roles = Role.Admin)]

        public async Task<IActionResult> CreateAsync(DisciplineDto request)
        {
            var id = await _disciplineService.CreateAsync(request);
            return Ok(id);
        }

        /// <summary>
        /// Удалить дисциплину
        /// </summary>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            if (!await _disciplineService.DeleteAsync(id))
                return NoContent();

            return Accepted();
        }
    }
}
