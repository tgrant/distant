﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Common;

namespace TGrant.Distant.Host.Controllers.v1.Admins
{
    /// <summary>
    /// Образовательные ветки доступные Админу
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class BranchController : ControllerBase
    {
        private readonly IBranchService _branchService;

        public BranchController(IBranchService branchService)
        {
            _branchService = branchService;
        }

        /// <summary>
        /// Получить все образовательные ветки
        /// </summary>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<BranchInfo>))]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> GetAllAsync()
        {
            var items = await _branchService.GetAllAsync();
            return Ok(items);
        }

        /// <summary>
        /// Получить образовательную ветку по id
        /// </summary>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BranchInfo))]
        [Authorize(Roles = Role.Admin)]

        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var item = await _branchService.GetByIdAsync(id);
            return Ok(item);
        }

        /// <summary>
        /// Обновить образовательную ветку
        /// </summary>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = Role.Admin)]

        public async Task<IActionResult> UpdateAsync(BranchDto request)
        {
            await _branchService.UpdateAsync(request);
            return Ok();
        }

        /// <summary>
        /// Создать образовательную ветку
        /// </summary>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(int))]
        [Authorize(Roles = Role.Admin)]

        public async Task<IActionResult> CreateAsync(BranchDto request)
        {
            var id = await _branchService.CreateAsync(request);
            return Ok(id);
        }

        /// <summary>
        /// Удалить образовательную ветку
        /// </summary>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            if (!await _branchService.DeleteAsync(id))
                return NoContent();

            return Accepted();
        }
    }
}
