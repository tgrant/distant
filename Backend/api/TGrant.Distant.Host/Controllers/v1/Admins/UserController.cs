﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Authentications;
using TGrant.Distant.Core.Common;

namespace TGrant.Distant.Host.Controllers.v1.Admins
{
    /// <summary>
    /// Пользователи доступные Админу
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Вернуть всех пользователей
        /// </summary>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<UserInfo>))]
        [Authorize(Roles = Role.Admin)]
        public async Task<ActionResult> GetAllAsync()
        {
            var users = await _userService.GetAllAsync();
            return Ok(users);
        }

        /// <summary>
        /// Создать пользователя
        /// </summary>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(int))]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> CreateAsync(UserDto request)
        {
            var id = await _userService.CreateAsync(request);
            return Ok(id);
        }
    }
}
