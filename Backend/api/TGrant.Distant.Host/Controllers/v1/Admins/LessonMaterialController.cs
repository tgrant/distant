﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Common;

namespace TGrant.Distant.Host.Controllers.v1.Admins
{
    /// <summary>
    /// Материалы уроков доступные Админу
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class LessonMaterialController : ControllerBase
    {
        private readonly ILessonMaterialService _lessonMaterialService;

        public LessonMaterialController(ILessonMaterialService lessonMaterialService)
        {
            _lessonMaterialService = lessonMaterialService;
        }

        /// <summary>
        /// Получить все материалы уроков
        /// </summary>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<LessonMaterialInfo>))]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> GetAllAsync()
        {
            var items = await _lessonMaterialService.GetAllAsync();
            return Ok(items);
        }

        /// <summary>
        /// Получить материалы уроков по уроку
        /// </summary>
        [HttpGet("{lessonId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<LessonMaterialInfo>))]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> GetByLessonIdAsync(int lessonId)
        {
            var items = await _lessonMaterialService.GetByLessonIdAsync(lessonId);
            return Ok(items);
        }

        /// <summary>
        /// Получить материал урока по id
        /// </summary>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(LessonMaterialInfo))]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var item = await _lessonMaterialService.GetByIdAsync(id);
            return Ok(item);
        }

        /// <summary>
        /// Обновить материал урока
        /// </summary>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> UpdateAsync(LessonMaterialDto request)
        {
            await _lessonMaterialService.UpdateAsync(request);
            return Ok();
        }

        /// <summary>
        /// Создать материал урока
        /// </summary>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(int))]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> CreateAsync(LessonMaterialDto request)
        {
            var id = await _lessonMaterialService.CreateAsync(request);
            return Ok(id);
        }

        /// <summary>
        /// Удалить материал урока
        /// </summary>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            if (!await _lessonMaterialService.DeleteAsync(id))
                return NoContent();

            return Accepted();
        }
    }
}
