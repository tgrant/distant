﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Common;
using TGrant.Distant.Host.Extensions;

namespace TGrant.Distant.Host.Controllers.v1.Takers
{
    /// <summary>
    /// Учебные курсы доступные Ученику
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TakerCourseProgramController : ControllerBase
    {
        private readonly ITakerCourseProgramService _takerCourseProgramService;

        public TakerCourseProgramController(ITakerCourseProgramService takerCourseProgramService)
        {
            _takerCourseProgramService = takerCourseProgramService;
        }

        /// <summary>
        /// Получить все программы курсов ученика
        /// </summary>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<CourseProgramInfo>))]
        [Authorize(Roles = Role.TakerAndAdmin)]
        public async Task<IActionResult> GetAllAsync()
        {
            var items = await _takerCourseProgramService.GetAllAsync(User.GetIdOrNull(Role.Taker));
            return Ok(items);
        }

        /// <summary>
        /// Получить программу курса ученика по id
        /// </summary>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CourseProgramInfo))]
        [Authorize(Roles = Role.TakerAndAdmin)]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var item = await _takerCourseProgramService.GetByIdAsync(id, User.GetIdOrNull(Role.Taker));
            return Ok(item);
        }
    }
}
