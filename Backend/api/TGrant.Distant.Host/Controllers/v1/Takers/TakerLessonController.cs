﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Common;
using TGrant.Distant.Host.Extensions;

namespace TGrant.Distant.Host.Controllers.v1.Takers
{
    /// <summary>
    /// Учебные курсы доступные Ученику
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TakerLessonController : ControllerBase
    {
        private readonly ITakerCourseProgramService _takerCourseProgramService;

        public TakerLessonController(ITakerCourseProgramService takerCourseProgramService)
        {
            _takerCourseProgramService = takerCourseProgramService;
        }

        /// <summary>
        /// Получить уроки курса ученика по id
        /// </summary>
        [HttpGet("{courseProgramId}/lessons")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<LessonInfo>))]
        [Authorize(Roles = Role.TakerAndAdmin)]
        public async Task<IActionResult> GetLessonsByCourseProgramIdAsync(int courseProgramId)
        {
            var item = await _takerCourseProgramService.GetLessonsByCourseProgramIdAsync(courseProgramId, User.GetIdOrNull(Role.Taker));
            return Ok(item);
        }

        /// <summary>
        /// Получить материалы урокаученика по id
        /// </summary>
        [HttpGet("{lessonId}/lessonmaterials")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<LessonMaterialInfo>))]
        [Authorize(Roles = Role.TakerAndAdmin)]
        public async Task<IActionResult> GetLessonMaterialssByCourseProgramIdAsync(int lessonId)
        {
            var item = await _takerCourseProgramService.GetLessonMaterialsByLessonIdAsync(lessonId, User.GetIdOrNull(Role.Taker));
            return Ok(item);
        }
    }
}
