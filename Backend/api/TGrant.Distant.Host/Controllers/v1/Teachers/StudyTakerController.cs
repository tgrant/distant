﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Common;
using TGrant.Distant.Host.Extensions;

namespace TGrant.Distant.Host.Controllers.v1.Teachers
{
    /// <summary>
    /// Ученики доступные Учителю
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StudyTakerController : ControllerBase
    {
        private readonly ITakerService _takerService;

        public StudyTakerController(ITakerService takerService)
        {
            _takerService = takerService;
        }

        /// <summary>
        /// Получить всех учеников группы
        /// </summary>
        [HttpGet("/group/{takerGroupId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<TakerInfo>))]
        [Authorize(Roles = Role.TeacherAndAdmin)]
        public async Task<IActionResult> GetAllByTakerGroupIdAsync(int takerGroupId)
        {
            var items = await _takerService.GetAllByTakerGroupIdAsync(takerGroupId, User.GetIdOrNull(Role.Teacher));
            return Ok(items);
        }

        /// <summary>
        /// Получить ученика по id
        /// </summary>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(TakerInfo))]
        [Authorize(Roles = Role.TeacherAndAdmin)]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var item = await _takerService.GetByIdAsync(id, User.GetIdOrNull(Role.Teacher));
            return Ok(item);
        }

        /// <summary>
        /// Обновить ученика
        /// </summary>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = Role.TeacherAndAdmin)]
        public async Task<IActionResult> UpdateAsync(TakerDto request)
        {
            await _takerService.UpdateAsync(request, User.GetIdOrNull(Role.Teacher));
            return Ok();
        }

        /// <summary>
        /// Создать ученика
        /// </summary>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(int))]
        [Authorize(Roles = Role.TeacherAndAdmin)]
        public async Task<IActionResult> CreateAsync(TakerDto request)
        {
            var id = await _takerService.CreateAsync(request, User.GetIdOrNull(Role.Teacher));
            return Ok(id);
        }

        /// <summary>
        /// Удалить ученика
        /// </summary>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [Authorize(Roles = Role.TeacherAndAdmin)]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            if (!await _takerService.DeleteAsync(id, User.GetIdOrNull(Role.Teacher)))
                return NoContent();

            return Accepted();
        }
    }
}
