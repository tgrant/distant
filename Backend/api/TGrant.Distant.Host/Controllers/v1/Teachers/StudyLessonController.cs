﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Common;
using TGrant.Distant.Host.Extensions;

namespace TGrant.Distant.Host.Controllers.v1.Teachers
{
    /// <summary>
    /// Уроки доступные Учителю
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StudyLessonController : ControllerBase
    {
        private readonly ILessonService _lessonService;

        public StudyLessonController(ILessonService lessonService)
        {
            _lessonService = lessonService;
        }

        /// <summary>
        /// Получить все уроки программы курса
        /// </summary>
        [HttpGet("{courseProgramId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<LessonInfo>))]
        [Authorize(Roles = Role.TeacherAndAdmin)]
        public async Task<IActionResult> GetByCourseProgramIdAsync(int courseProgramId)
        {
            var items = await _lessonService.GetByCourseProgramIdAsync(courseProgramId, User.GetIdOrNull(Role.Teacher));
            return Ok(items);
        }

        /// <summary>
        /// Получить урок по id
        /// </summary>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(LessonInfo))]
        [Authorize(Roles = Role.TeacherAndAdmin)]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var item = await _lessonService.GetByIdAsync(id, User.GetIdOrNull(Role.Teacher));
            return Ok(item);
        }
    }
}
