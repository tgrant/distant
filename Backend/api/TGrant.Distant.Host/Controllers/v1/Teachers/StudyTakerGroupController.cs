﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Common;
using TGrant.Distant.Host.Extensions;

namespace TGrant.Distant.Host.Controllers.v1.Teachers
{
    /// <summary>
    /// Учебные группы доступные Учителю
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StudyTakerGroupController : ControllerBase
    {
        private readonly ITakerGroupService _takerGroupService;

        public StudyTakerGroupController(ITakerGroupService takerGroupService)
        {
            _takerGroupService = takerGroupService;
        }

        /// <summary>
        /// Получить все группы учителя
        /// </summary>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<TakerGroupInfo>))]
        [Authorize(Roles = Role.TeacherAndAdmin)]
        public async Task<IActionResult> GetAllAsync()
        {
            var items = await _takerGroupService.GetAllAsync(User.GetIdOrNull(Role.Teacher));
            return Ok(items);
        }

        /// <summary>
        /// Получить группу учителя по программе курса
        /// </summary>
        [HttpGet("{courseProgramId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<TakerGroupInfo>))]
        [Authorize(Roles = Role.TeacherAndAdmin)]
        public async Task<IActionResult> GetAllByCourseProgramIdAsync(int courseProgramId)
        {
            var item = await _takerGroupService.GetAllByCourseProgramIdAsync(courseProgramId, User.GetIdOrNull(Role.Teacher));
            return Ok(item);

        }

        /// <summary>
        /// Получить группу учителя по id
        /// </summary>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(TakerGroupInfo))]
        [Authorize(Roles = Role.TeacherAndAdmin)]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var item = await _takerGroupService.GetByIdAsync(id, User.GetIdOrNull(Role.Teacher));
            return Ok(item);
        }

        /// <summary>
        /// Обновить группу
        /// </summary>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = Role.TeacherAndAdmin)]
        public async Task<IActionResult> UpdateAsync(TakerGroupDto request)
        {
            await _takerGroupService.UpdateAsync(request, User.GetIdOrNull(Role.Teacher));
            return Ok();
        }

        /// <summary>
        /// Создать группу
        /// </summary>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(int))]
        [Authorize(Roles = Role.TeacherAndAdmin)]
        public async Task<IActionResult> CreateAsync(TakerGroupDto request)
        {
            var id = await _takerGroupService.CreateAsync(request, User.GetIdOrNull(Role.Teacher));
            return Ok(id);
        }

        /// <summary>
        /// Удалить группу
        /// </summary>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [Authorize(Roles = Role.TeacherAndAdmin)]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            if (!await _takerGroupService.DeleteAsync(id, User.GetIdOrNull(Role.Teacher)))
                return NoContent();

            return Accepted();
        }
    }
}
