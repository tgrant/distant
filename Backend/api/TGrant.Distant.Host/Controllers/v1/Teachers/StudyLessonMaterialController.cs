﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Common;
using TGrant.Distant.Host.Extensions;

namespace TGrant.Distant.Host.Controllers.v1.Teachers
{
    /// <summary>
    /// Материалы урока доступные Учителю
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StudyLessonMaterialController : ControllerBase
    {
        private readonly ILessonMaterialService _lessonMaterialService;

        public StudyLessonMaterialController(ILessonMaterialService lessonMaterialService)
        {
            _lessonMaterialService = lessonMaterialService;
        }

        /// <summary>
        /// Получить все материалы урока
        /// </summary>
        [HttpGet("{courseProgramId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<LessonMaterialInfo>))]
        [Authorize(Roles = Role.TeacherAndAdmin)]
        public async Task<IActionResult> GetByLessonIdAsync(int lessonId)
        {
            var items = await _lessonMaterialService.GetByLessonIdAsync(lessonId, User.GetIdOrNull(Role.Teacher));
            return Ok(items);
        }

        /// <summary>
        /// Получить материал урока по id
        /// </summary>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(LessonMaterialInfo))]
        [Authorize(Roles = Role.TeacherAndAdmin)]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var item = await _lessonMaterialService.GetByIdAsync(id, User.GetIdOrNull(Role.Teacher));
            return Ok(item);
        }
    }
}
