﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Admins;
using TGrant.Distant.Core.Common;
using TGrant.Distant.Host.Extensions;

namespace TGrant.Distant.Host.Controllers.v1.Teachers
{
    /// <summary>
    /// Учебные курсы доступные Учителю
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StudyCourseController : ControllerBase
    {
        private readonly ICourseProgramService _courseProgramService;

        public StudyCourseController(ICourseProgramService courseProgramService)
        {
            _courseProgramService = courseProgramService;
        }

        /// <summary>
        /// Получить все программы курсов учителя
        /// </summary>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<CourseProgramInfo>))]
        [Authorize(Roles = Role.TeacherAndAdmin)]
        public async Task<IActionResult> GetAllAsync()
        {
            var items = await _courseProgramService.GetAllAsync(User.GetIdOrNull(Role.Teacher));
            return Ok(items);
        }

        /// <summary>
        /// Получить программу курса учителя по id
        /// </summary>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CourseProgramInfo))]
        [Authorize(Roles = Role.TeacherAndAdmin)]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var item = await _courseProgramService.GetByIdAsync(id, User.GetIdOrNull(Role.Teacher));
            return Ok(item);
        }
    }
}
