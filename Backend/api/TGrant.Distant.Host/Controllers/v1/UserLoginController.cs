﻿using Microsoft.AspNetCore.Mvc;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Contracts.Authentications;

namespace TGrant.Distant.Host.Controllers.v1
{
    /// <summary>
    /// Логин
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UserLoginController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserLoginController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Login user
        /// </summary>
        /// <param name="loginRequest"> Dto: <see cref="LoginDto"/>
        /// <returns></returns>
        /// <example> Например:
        /// { "userName" : "admin", "password" : "adminpwd"}
        /// </example></param>
        [HttpPost]
        public async Task<ActionResult<UserInfo>> Login(LoginDto loginRequest)
        {
            if (string.IsNullOrEmpty(loginRequest.Username) || string.IsNullOrEmpty(loginRequest.Password))
                return BadRequest("Username and/or Password not specified");

            var userInfo = await _userService.GetAsync(loginRequest.Username, loginRequest.Password);

            if (userInfo == null)
                return BadRequest("User with such name and password not found");

            return Ok(userInfo);
        }
    }
}
