﻿using Microsoft.EntityFrameworkCore;
using TGrant.Distant.Core.Common;
using TGrant.Distant.Core.Entities;
using TGrant.Distant.Core.Enums;

namespace TGrant.Distant.Infrastructure.Persistence
{
    public static class ApplicationDbContextSeed
    {
        public static async Task Seed(ApplicationDbContext context)
        {
            context.Database.Migrate();

            if (context.User.Any())
                return;

            context.User.AddRange(
            new List<User>
            {
                    new User("admin", "adminpwd", "Иван", "Иванов", Role.Admin),
                    new User("teacher", "teacherpwd", "Петр", "Петров", Role.Teacher),
                    new User("taker1", "takerpwd", "Сидр", "Сидоров", Role.Taker),
                    new User("taker2", "takerpwd", "Олег", "Тинькоф", Role.Taker),
                    new User("taker3", "takerpwd", "Олеся", "Ульянова", Role.Taker),
                    new User("taker4", "takerpwd", "Иосиф", "Шварц", Role.Taker),
            });

            var branches = new List<Branch>
            {
                new Branch("ЧОП", "обучение охранников", true),
                new Branch("Автошкола", "обучение вождению", true),
            };
            context.Branch.AddRange(branches);

            var course1 = new Course("Охранник первого разряда", "охранник супермаркетов", true);
            var course2 = new Course("Спецохранник", "крутой ковбой", true);
            var course3 = new Course("Водитель категории А", "курс обучение вождению категории А", true);
            var course4 = new Course("Водитель категории B", "курс обучение вождению категории B", true);

            branches[0].Courses.Add(course1);
            branches[0].Courses.Add(course2);
            branches[1].Courses.Add(course3);
            branches[1].Courses.Add(course4);

            var courseProgram1 = new CourseProgram("Программа 1", "новая программа 2022", 1);
            var courseProgram2 = new CourseProgram("Программа 2022", "новая программа", 1);
            course1.CoursePrograms.Add(courseProgram1);
            course1.CoursePrograms.Add(new CourseProgram("Программа 2021", "старая программа", 1));
            course4.CoursePrograms.Add(new CourseProgram("Программа 2021", " программа", 1));
            course4.CoursePrograms.Add(courseProgram2);

            var disciplineChop1 = new Discipline("Владение оружием", "курс обучение охранников владению оружием");
            var disciplineChop2 = new Discipline("Рукопашный бой", "обучение рукопашному бою");
            var disciplineChop3 = new Discipline("Первая помощь", "обучение оказанию первой помощи");
            var disciplineChop4 = new Discipline("Права и обязанности частного охранника", "обучение основам права и обязанности частного охранника");
            var disciplineDriver1 = new Discipline("Теория вождения", "курс обучение теории вождения");
            var disciplineDriver2 = new Discipline("Дорожные знаки", "обучение дорожным знакам");
            var disciplineDriver3 = new Discipline("Практика", "практическое вождение");
            courseProgram1.Disciplines.Add(disciplineChop1);
            courseProgram1.Disciplines.Add(disciplineChop2);
            courseProgram1.Disciplines.Add(disciplineChop3);
            courseProgram1.Disciplines.Add(disciplineChop4);
            courseProgram2.Disciplines.Add(disciplineDriver1);
            courseProgram2.Disciplines.Add(disciplineDriver2);
            courseProgram2.Disciplines.Add(disciplineDriver3);

            var lesson1 = new Lesson("Урок 1", "Оружие - средство повышенной опасности");
            disciplineChop1.Lessons.Add(lesson1);
            disciplineChop1.Lessons.Add(new Lesson("Урок 2", "Типы оружия"));
            disciplineChop1.Lessons.Add(new Lesson("Урок 3", "Холодное оружие"));
            disciplineChop1.Lessons.Add(new Lesson("Урок 4", "Огнестрельное оружие"));
            disciplineChop1.Lessons.Add(new Lesson("Урок 5", "Использование оружия для самозащиты"));

            disciplineChop2.Lessons.Add(new Lesson("Урок 1", "Виды рукопашного боя"));
            disciplineChop2.Lessons.Add(new Lesson("Урок 2", "Самбо"));
            disciplineChop2.Lessons.Add(new Lesson("Урок 3", "Дзюдо"));
            disciplineChop2.Lessons.Add(new Lesson("Урок 4", "Тхеквандо"));
            disciplineChop2.Lessons.Add(new Lesson("Урок 5", "Айкидо"));
            disciplineChop2.Lessons.Add(new Lesson("Урок 6", "Бег трусцой"));

            disciplineDriver1.Lessons.Add(new Lesson("Урок 1", "Теория вождения - проезжая часть"));
            disciplineDriver1.Lessons.Add(new Lesson("Урок 2", "Теория вождения - помеха справа"));
            disciplineDriver1.Lessons.Add(new Lesson("Урок 3", "Теория вождения - обгон"));
            disciplineDriver1.Lessons.Add(new Lesson("Урок 4", "Теория вождения - на перекрестке"));
            disciplineDriver1.Lessons.Add(new Lesson("Урок 5", "Теория вождения - опасный поворот"));

            disciplineDriver2.Lessons.Add(new Lesson("Урок 1", "Дорожные знаки - запрещающие"));
            disciplineDriver2.Lessons.Add(new Lesson("Урок 2", "Дорожные знаки - предупреждающие"));
            disciplineDriver2.Lessons.Add(new Lesson("Урок 3", "Дорожные знаки - знаки приоритета"));
            disciplineDriver2.Lessons.Add(new Lesson("Урок 4", "Дорожные знаки - предписывающие"));
            disciplineDriver2.Lessons.Add(new Lesson("Урок 5", "Дорожные знаки - информационные"));

            await context.SaveChangesAsync();

            lesson1.LessonMaterials.Add(new LessonMaterial(LessonMaterialType.File, "конспект урока"));
            lesson1.LessonMaterials.Add(new LessonMaterial(LessonMaterialType.Video, "запись урока", "https://somevideoservice/video?id=1"));
            lesson1.LessonMaterials.Add(new LessonMaterial(LessonMaterialType.Quiz, "пройдите опрос по уроку", "https://somequizservice/quiz?id=1"));

            await context.SaveChangesAsync();

            var takerGroup1 = new TakerGroup(context.CourseProgram.FirstOrDefault(), "Группа А1");
            var takerGroup2 = new TakerGroup(context.CourseProgram.FirstOrDefault(), "Группа А2");
            var takerGroup3 = new TakerGroup(context.CourseProgram.FirstOrDefault(), "Группа Б1");

            context.TakerGroup.AddRange(new List<TakerGroup>
            {
                takerGroup1,
                takerGroup2,
                takerGroup3,
            });

            var users = context.User.Where(x => x.Role == Role.Taker).ToList();
            context.Taker.AddRange(new List<Taker>
            {
                new Taker(users[0], takerGroup1),
                new Taker(users[1], takerGroup1),
                new Taker(users[2], takerGroup1),
                new Taker(users[3], takerGroup2),
            });

            await context.SaveChangesAsync();

        }
    }
}
