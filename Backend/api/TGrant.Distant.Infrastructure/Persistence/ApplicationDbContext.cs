﻿using Microsoft.EntityFrameworkCore;
using System.Reflection;
using TGrant.Distant.Application.Interfaces;
using TGrant.Distant.Core.Entities;

namespace TGrant.Distant.Infrastructure.Persistence
{
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        public DbSet<Branch> Branch { get; set; }

        public DbSet<Course> Course { get; set; }

        public DbSet<CourseProgram> CourseProgram { get; set; }

        public DbSet<Discipline> Discipline { get; set; }

        public DbSet<Lesson> Lesson { get; set; }

        public DbSet<LessonMaterial> LessonMaterial { get; set; }

        public DbSet<Taker> Taker { get; set; }

        public DbSet<TakerGroup> TakerGroup { get; set; }

        public DbSet<User> User { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            var result = await base.SaveChangesAsync(cancellationToken);

            return result;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(builder);
        }
    }
}
