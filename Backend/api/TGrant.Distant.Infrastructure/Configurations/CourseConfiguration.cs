﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TGrant.Distant.Core.Entities;

namespace TGrant.Distant.Infrastructure.Configurations
{
    internal class CourseConfiguration : IEntityTypeConfiguration<Course>
    {
        public void Configure(EntityTypeBuilder<Course> builder)
        {
            builder.Property(t => t.Name).HasMaxLength(100).IsRequired();
            builder.HasMany(p => p.CoursePrograms).WithOne(p => p.Course).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
