﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TGrant.Distant.Core.Entities;

namespace TGrant.Distant.Infrastructure.Configurations
{
    internal class LessonMaterialConfiguration : IEntityTypeConfiguration<LessonMaterial>
    {
        public void Configure(EntityTypeBuilder<LessonMaterial> builder)
        {
            builder.Property(t => t.Name).HasMaxLength(100).IsRequired();
        }
    }
}
