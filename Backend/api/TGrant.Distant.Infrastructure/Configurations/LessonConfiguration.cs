﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TGrant.Distant.Core.Entities;

namespace TGrant.Distant.Infrastructure.Configurations
{
    internal class LessonConfiguration : IEntityTypeConfiguration<Lesson>
    {
        public void Configure(EntityTypeBuilder<Lesson> builder)
        {
            builder.Property(t => t.Name).HasMaxLength(100).IsRequired();
            builder.HasMany(p => p.LessonMaterials).WithOne(p => p.Lesson).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
