﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TGrant.Distant.Core.Entities;

namespace TGrant.Distant.Infrastructure.Configurations
{
    internal class CourseProgramConfiguration : IEntityTypeConfiguration<CourseProgram>
    {
        public void Configure(EntityTypeBuilder<CourseProgram> builder)
        {
            builder.Property(t => t.Name).HasMaxLength(100).IsRequired();
            builder.HasMany(p => p.Disciplines).WithOne(p => p.CourseProgram).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
