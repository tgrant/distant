﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TGrant.Distant.Core.Entities;

namespace TGrant.Distant.Infrastructure.Configurations
{
    internal class DisciplineConfiguration : IEntityTypeConfiguration<Discipline>
    {
        public void Configure(EntityTypeBuilder<Discipline> builder)
        {
            builder.Property(t => t.Name).HasMaxLength(100).IsRequired();
            builder.HasMany(p => p.Lessons).WithOne(p => p.Discipline).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
