﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TGrant.Distant.Core.Entities;

namespace TGrant.Distant.Infrastructure.Configurations
{
    internal class TakerGroupConfiguration : IEntityTypeConfiguration<TakerGroup>
    {
        public void Configure(EntityTypeBuilder<TakerGroup> builder)
        {
            builder.Property(t => t.Name).HasMaxLength(100).IsRequired();
            builder.HasMany(p => p.Takers).WithOne(p => p.TakerGroup).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
