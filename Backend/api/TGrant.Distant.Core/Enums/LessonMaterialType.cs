﻿namespace TGrant.Distant.Core.Enums
{
    public enum LessonMaterialType
    {
        None,
        File,
        Video,
        Quiz
    }
}
