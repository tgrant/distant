﻿namespace TGrant.Distant.Core.Common
{
    public static class Role
    {
        public const string User = "User";
        public const string Admin = "Admin";
        public const string Taker = "Taker";
        public const string Teacher = "Teacher";
        public const string TeacherAndAdmin = $"{Admin}, {Teacher}";
        public const string TakerAndAdmin = $"{Admin}, {Taker}";
    }
}
