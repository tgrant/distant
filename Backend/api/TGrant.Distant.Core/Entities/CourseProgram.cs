﻿using TGrant.Distant.Core.Common;

namespace TGrant.Distant.Core.Entities
{
    public class CourseProgram : Entity
    {
        public string Name { get; private set; }

        public string Description { get; private set; }

        public int? UserId { get; private set; }

        public bool IsActive { get; private set; }

        public virtual Course Course { get; private set; }

        public virtual ICollection<Discipline> Disciplines { get; private set; } = new List<Discipline>();

        protected CourseProgram()
        {
        }

        public CourseProgram(string name, string description, int? userId, bool isActive = true)
        {
            Name = name;
            Description = description;
            IsActive = isActive;
            UserId = userId;
        }

        public void Update(string name, string description, int? userId, bool isActive)
        {
            Name = name;
            Description = description;
            IsActive = isActive;
            UserId = userId;
        }
    }
}
