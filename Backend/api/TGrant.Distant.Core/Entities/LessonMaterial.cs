﻿using TGrant.Distant.Core.Common;
using TGrant.Distant.Core.Enums;

namespace TGrant.Distant.Core.Entities
{
    public class LessonMaterial : Entity
    {
        public string Name { get; private set; }

        public LessonMaterialType Type { get; private set; }

        public string Link { get; private set; }


        public virtual Lesson Lesson { get; private set; }

        protected LessonMaterial()
        {
        }

        public LessonMaterial(LessonMaterialType type, string name, string link = default)
        {
            Type = type;
            Name = name;
            Link = link ?? "";
        }

        public void Update(LessonMaterialType type, string name, string link = default)
        {
            Type = type;
            Name = name;
            Link = link ?? "";
        }
    }
}
