﻿using TGrant.Distant.Core.Common;

namespace TGrant.Distant.Core.Entities
{
    public class User : Entity
    {
        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public string Username { get; private set; }

        public string Password { get; private set; }

        public string Role { get; private set; }

        protected User()
        {
        }

        public User(string username, string password, string firstName, string lastName, string role)
        {
            FirstName = firstName;
            LastName = lastName;
            Username = username;
            Password = password;
            Role = role;
        }
    }
}
