﻿using TGrant.Distant.Core.Common;

namespace TGrant.Distant.Core.Entities
{
    public class Lesson : Entity
    {
        public string Name { get; private set; }

        public string Description { get; private set; }

        public virtual Discipline Discipline { get; private set; }

        public virtual ICollection<LessonMaterial> LessonMaterials { get; private set; } = new List<LessonMaterial>();

        protected Lesson()
        {
        }

        public Lesson(string name, string description)
        {
            Name = name;
            Description = description;
        }

        public void Update(string name, string description)
        {
            Name = name;
            Description = description;
        }
    }
}
