﻿using TGrant.Distant.Core.Common;

namespace TGrant.Distant.Core.Entities
{
    public class Discipline : Entity
    {
        public string Name { get; private set; }

        public string Description { get; set; }

        public virtual CourseProgram CourseProgram { get; private set; }

        public virtual ICollection<Lesson> Lessons { get; private set; } = new List<Lesson>();

        protected Discipline()
        {
        }

        public Discipline(string name, string description)
        {
            Name = name;
            Description = description;
        }

        public void Update(string name, string description)
        {
            Name = name;
            Description = description;
        }
    }
}
