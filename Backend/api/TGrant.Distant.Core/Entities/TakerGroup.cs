﻿using TGrant.Distant.Core.Common;

namespace TGrant.Distant.Core.Entities
{
    public class TakerGroup : Entity
    {
        public string Name { get; private set; }

        public bool IsActive { get; private set; }

        public virtual CourseProgram CourseProgram { get; private set; }

        public virtual ICollection<Taker> Takers { get; private set; } = new HashSet<Taker>();

        protected TakerGroup()
        {
        }

        public TakerGroup(CourseProgram courseProgram, string name, bool isActive = true)
        {
            CourseProgram = courseProgram;
            Name = name;
            IsActive = isActive;
        }

        public void Update(string name, bool isActive)
        {
            Name = name;
            IsActive = isActive;
        }
    }
}
