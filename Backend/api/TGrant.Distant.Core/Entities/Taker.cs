﻿using TGrant.Distant.Core.Common;

namespace TGrant.Distant.Core.Entities
{
    public class Taker : Entity
    {
        public virtual User User { get; private set; }

        public virtual TakerGroup TakerGroup { get; private set; }

        protected Taker()
        {
        }

        public Taker(User user, TakerGroup takerGroup)
        {
            User = user;
            TakerGroup = takerGroup;
        }

        public void Update(TakerGroup takerGroup)
        {
            TakerGroup = takerGroup;
        }
    }
}
