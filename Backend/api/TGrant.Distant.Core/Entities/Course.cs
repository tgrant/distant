﻿using TGrant.Distant.Core.Common;

namespace TGrant.Distant.Core.Entities
{
    public class Course : Entity
    {
        public string Name { get; private set; }

        public string Description { get; private set; }

        public bool IsActive { get; private set; }

        public virtual Branch Branch { get; private set; }

        public virtual ICollection<CourseProgram> CoursePrograms { get; private set; } = new HashSet<CourseProgram>();

        protected Course()
        {
        }

        public Course(string name, string description, bool isActive)
        {
            Name = name;
            Description = description;
            IsActive = isActive;
        }

        public void Update(string name, string description, bool isActive)
        {
            Name = name;
            Description = description;
            IsActive = isActive;
        }
    }
}
