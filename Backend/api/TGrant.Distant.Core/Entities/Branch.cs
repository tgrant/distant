﻿using TGrant.Distant.Core.Common;

namespace TGrant.Distant.Core.Entities
{
    public class Branch : Entity
    {
        public string Name { get; private set; }

        public string Description { get; private set; }

        public bool IsActive { get; private set; }

        public virtual ICollection<Course> Courses { get; private set; } = new List<Course>();

        protected Branch()
        {
        }

        public Branch(string name, string description, bool isActive)
        {
            Name = name;
            Description = description;
            IsActive = isActive;
        }

        public void Update(string name, string description, bool isActive)
        {
            Name = name;
            Description = description;
            IsActive = isActive;
        }
    }
}
